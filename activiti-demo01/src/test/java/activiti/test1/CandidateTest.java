package activiti.test1;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CandidateTest {

    //  测试候选人
    @Test
    public void deploy(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deploy = repositoryService.createDeployment()
                .name("测试候选人1")
                .addClasspathResource("flow/candidate.bpmn20.xml")
                .deploy();
        System.out.println("流程部署id：" + deploy.getId());
        System.out.println("流程部署Name：" + deploy.getName());
    }

    @Test
    public void start(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();

        runtimeService.startProcessInstanceByKey("candidateId");
    }

    // 查询组任务
    // 通过候选人查询

    /**
     * SELECT DISTINCT
     * 	RES.*
     * FROM
     * 	ACT_RU_TASK RES
     * 	INNER JOIN ACT_RU_IDENTITYLINK I ON I.TASK_ID_ = RES.ID_
     * 	INNER JOIN ACT_RE_PROCDEF D ON RES.PROC_DEF_ID_ = D.ID_
     * WHERE
     * 	D.KEY_ = 'candidateId'
     * 	AND RES.ASSIGNEE_ IS NULL
     * 	AND I.TYPE_ = 'candidate'
     * 	AND ( I.USER_ID_ = 'ww' )
     * ORDER BY
     * 	RES.ID_ ASC
     * 	LIMIT 2147483647 OFFSET 0
     */
    @Test
    public void findGroupTaskList(){

        // 任务候选人
        String candidateUser = "ww";

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("candidateId")
                .taskCandidateUser(candidateUser)
                .list();
        for (Task task : list) {
            System.out.println("流程实例id：" + task.getProcessInstanceId());
            System.out.println("任务id：" + task.getId());
            System.out.println("任务负责人：" + task.getAssignee());
            System.out.println("===========================================");
        }
    }
    // 候选人拾取任务
    @Test
    public void claimtask(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();

        // 先查询到任务
        Task task = taskService.createTaskQuery()
                .taskId("22505")            // 任务id
                .taskCandidateUser("ww")  //候选人
                .singleResult();

        if (task != null) {
            taskService.claim("22505","ww");  // 拾取任务
        }
    }
    // 归还任务
    @Test
    public void backTask(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .taskId("22505")
                .taskAssignee("ww")
                .singleResult();
        if (task != null) {
            taskService.setAssignee("22505",null);  // 归还任务->其实就是将负责人设置为空
        }
    }

    //任务交接 -》 其实就是改变任务的负责人
    @Test
    public void changeUser(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .taskId("22505")
                .taskAssignee("ww")
                .singleResult();
        if (task != null) {
            taskService.setAssignee("22505","ls");  // 交接
        }
    }

}
