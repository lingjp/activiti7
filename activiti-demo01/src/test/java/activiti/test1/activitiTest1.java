package activiti.test1;

import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class activitiTest1 {

    // 获取 ProcessEngine 对象的第一种方式
    // 通过getDefaultProcessEngine()方法获取流程引擎对象，会加载resources目录下的activiti.cfg.xml
    @Test
    public void aa() {
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        System.out.println(defaultProcessEngine);
    }

    /**
     * 流程部署的操作
     * ACT_RE_PROCDEF
     * ACT_RE_DEOLOYMENT
     * ACT_GE_BYTEARRAY
     *
     */
    @Test
    public void flow1() throws FileNotFoundException {
        // 1. 获取ProcessEngine对象
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        // 2. 完成流程的部署操作 需要通过RepositoryService完成
        RepositoryService repositoryService = defaultProcessEngine.getRepositoryService();
        // 3. 完成部署操作
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("flow/test1.bpmn20.xml") // 通过xml
                //通过stream流
//                .addInputStream("test",new FileInputStream("D:\\Lingjp\\app\\projects\\idea_works\\activiti7\\activiti-demo01\\src\\main\\resources\\flow\\test1.bpmn20.xml"))
                .name("第一个流程")
                .deploy();
        System.out.println(deploy.getId());
        System.out.println(deploy.getName());

    }

    /**
     * 查询当前部署的流程有哪些
     */
    @Test
    public void selectflow() {
        // 1. 获取ProcessEngine对象
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = defaultProcessEngine.getRepositoryService();
        // 查询当前部署的流程有哪些 ==》 查询相关的流程定义信息
//        repositoryService.createDeploymentQuery(); // 查询流程部署的相关信息
//        repositoryService.createNativeDeploymentQuery();// 查询部署流程的相关定义
        List<Deployment> list = repositoryService.createDeploymentQuery().list();

        for (Deployment deployment : list) {
            System.out.println(deployment.getId());
            System.out.println(deployment.getName());
        }
    }
    /**
     * 发起一个流程
     * act_hi_taskinst  任务实例
     * act_hi_procinst  流程实例
     * act_hi_actinst  历史流程实例
     * act_ru_execytion 运行时执行
     * act_ru_task  运行是任务
     */
    @Test
    public void sartflow() {
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        // 发起流程需要通过getRuntimeService来实现
        RuntimeService runtimeService = defaultProcessEngine.getRuntimeService();
        // 通过流程定义id来启动流程 返回的是流程实例对象
        ProcessInstance processInstance = runtimeService.startProcessInstanceById("test1:1:3");
        System.out.println(processInstance.getId());
        System.out.println(processInstance.getDeploymentId());
        System.out.println(processInstance.getDescription());
        /**
         * 部署流程成功后就可以发起一个流程，发起流程需要通过RuntimeService来实现，
         * 流程发起后，在对应的act_ru_task中就会多一条待办记录
         */
    }

    /**
     * 代办的查询
     */

    @Test
    public void selecttask() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        List<Task> list = engine.getTaskService().createTaskQuery().taskAssignee("lisi").list();
        if (list != null && list.size()>0) {
            for (Task task : list) {
                System.out.println(task.getId());
                System.out.println(task.getName());
                System.out.println(task.getAssignee());
            }
        }else{
            System.out.println("当前没有代办任务");
        }
    }

    /**
     * 查询到待办任务之后 进行任务审批
     *   insert into act_hi_actinst
     *   delete from act_ru_task
     *   delete from act_run_execution
     */

    @Test
    public void taskshenpi() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = engine.getTaskService();
        // 原理上 根据当前登录用户查询出对应的待办信息
        // 完成任务（这里直接拿待办id直接完成）
        taskService.complete("2505");
    }


    /**
     * 查询历史流程
     */
    @Test
    public void checkHisProcessinst() {
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        HistoryService historyService = defaultProcessEngine.getHistoryService();
        HistoricProcessInstanceQuery historicProcessInstanceQuery = historyService.createHistoricProcessInstanceQuery();
        List<HistoricProcessInstance> list = historicProcessInstanceQuery.list();
        for (HistoricProcessInstance historicProcessInstance : list) {
            System.out.println(historicProcessInstance.getId());
            System.out.println(historicProcessInstance.getProcessDefinitionId());
            System.out.println(historicProcessInstance.getBusinessKey());
        }

    }
    /**
     * 查询历史流程任务
     */
    @Test
    public void checkHisProcessinstTask() {
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        HistoryService historyService = defaultProcessEngine.getHistoryService();
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().list();
        for (HistoricTaskInstance historicTaskInstance : list) {
            System.out.println(historicTaskInstance.getProcessInstanceId());
            System.out.println(historicTaskInstance.getStartTime());
            System.out.println(historicTaskInstance.getEndTime());
            System.out.println(historicTaskInstance.getName());
        }
    }

    /**
     * 查询历史活动中的实例
     */
    @Test
    public void checkActInst() {
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        HistoryService historyService = defaultProcessEngine.getHistoryService();
        List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery().list();
        for (HistoricActivityInstance historicActivityInstance : list) {
            System.out.println(historicActivityInstance.getProcessDefinitionId());
            System.out.println(historicActivityInstance.getActivityId());
            System.out.println(historicActivityInstance.getActivityName());
            System.out.println(historicActivityInstance.getActivityType());
        }
    }


}
