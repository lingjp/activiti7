package activiti.test1;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.junit.jupiter.api.Test;

import java.util.List;

public class backTest {


    @Test
    public void backdemo01(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        Deployment deploy = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("flow/back.bpmn20.xml")
                .name("测试回退3").deploy();
    }

    @Test
    public void backdemo02(){
        // 发起流程
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = defaultProcessEngine.getRuntimeService();
        ProcessInstance processInstance = runtimeService.startProcessInstanceById("back:2:15003");

    }

    @Test
    public void backdemo03(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance processInstance = runtimeService.startProcessInstanceById("back:1:3");
        TaskQuery taskQuery = processEngine.getTaskService().createTaskQuery().processDefinitionId("back:1:3");

        for (Task task : taskQuery.list()) {
            System.out.println(task.getId());
        }

    }
    @Test
    public void backdemo04() {
//        32505
//35005

        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = defaultProcessEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery().processDefinitionId("back:1:30003").list();
        for (Task task : list) {
            taskService.complete(task.getId()); // 执行任务

        }
    }


}
