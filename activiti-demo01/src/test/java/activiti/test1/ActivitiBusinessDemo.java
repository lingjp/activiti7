package activiti.test1;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ActivitiBusinessDemo {


    /**
     * 添加业务key到activiti中
     */
    @Test
    public void  a(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("evection", "1001");
        System.out.println(processInstance.getBusinessKey());
    }

    // 全部流程实例的挂起
    // 流程定义的暂停和激活
    // act_ru_task、act_ru_execution、act_re_procdef
    public void suspensionAll(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        List<ProcessDefinition> evenction = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("evenction")
                .list();
        // 全部流程实例的挂起
        for (ProcessDefinition processDefinition : evenction) {

            if (processDefinition.isSuspended()) {//判断是否被暂停
                //激活状态需要暂停
                // 第一个参数：流程id；
                // 第二个：true 激活，false 挂起
                // 第三个： 挂起、激活的时间
                repositoryService.activateProcessDefinitionById(processDefinition.getId(),false,null);
            }else{
                repositoryService.activateProcessDefinitionById(processDefinition.getId(),true,null);

            }

        }
    }


    // 单个流程实例的挂起、激活
    // 挂起的任务 没法完成
    public void suspensionSingle(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId("")// 实例id
                .singleResult();
        if (processInstance.isSuspended()) {
            String id = processInstance.getId();
            runtimeService.activateProcessInstanceById(id); // 使用给定的id 激活或挂起流程
        }else{
            String id = processInstance.getId();
            runtimeService.activateProcessInstanceById(id);
        }
    }
}
