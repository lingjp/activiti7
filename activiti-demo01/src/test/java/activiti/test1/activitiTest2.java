package activiti.test1;

import org.activiti.engine.*;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.reactive.server.StatusAssertions;

import java.util.List;

public class activitiTest2 {

    @Test
    public void test0() {
        // 拿到ProcessEngine 会通过activiti.cfg.xml拿到StandaloneProcessEngineConfiguration来创建ProcessEngine(流程引擎)
        //ProcessEngines 用于在服务器环境中初始化和关闭 流程引擎 的帮助程序。 也就是个工具类
        //先构建ProcessEngineConfiguration
//        ProcessEngineConfiguration configuration = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml");
        //通过ProcessEngineConfiguration创建ProcessEngine，此时会创建数据库
//        ProcessEngine processEngine = configuration.buildProcessEngine();
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        System.out.println(defaultProcessEngine);
    }


    @Test
    public void test1() {
        // 流程部署
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        System.out.println(defaultProcessEngine);

        // Deployment 代表了一个流程定义的部署单元，当把test1.bpmn20.xml部署到activiti引擎时，引擎会创建一个 Deployment 对象来代表这次部署操作。
        //
        Deployment deployment = defaultProcessEngine.getRepositoryService()
                .createDeployment()
                .addClasspathResource("flow/test1.bpmn20.xml")
                .name("请假流程1")
                .deploy();
        System.out.println(deployment.getId());
    }

    @Test
    public void test2() {
        // 发起流程
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = defaultProcessEngine.getRuntimeService();
        ProcessInstance processInstance = runtimeService.startProcessInstanceById("back:2:15003");
    }

    @Test
    public void test21() {
        // 查找待办任务
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        TaskQuery taskQuery = defaultProcessEngine.getTaskService().createTaskQuery().processDefinitionId("back:2:15003");

        for (Task task : taskQuery.list()) {
            System.out.println(task.getName());
        }
    }


    @Test
    public void test3() {
        // 完成任务
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = defaultProcessEngine.getTaskService();
        Task task = taskService.createTaskQuery().processDefinitionId("back:2:15003").singleResult();
        taskService.complete(task.getId()); // 执行任务
    }
    @Test
    public void test4() {
        // 查询历史流程
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        HistoricProcessInstanceQuery historicProcessInstanceQuery = defaultProcessEngine.getHistoryService().createHistoricProcessInstanceQuery();
        List<HistoricProcessInstance> list = historicProcessInstanceQuery.list();
        for (HistoricProcessInstance historicProcessInstance : list) {
            System.out.println(historicProcessInstance.getId());
        }

    }

}


