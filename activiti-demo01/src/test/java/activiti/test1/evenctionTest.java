package activiti.test1;

import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;

import java.util.List;

public class evenctionTest {

    @Test
    public void bushu(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        RepositoryService repositoryService = processEngine.getRepositoryService();

        Deployment deploy = repositoryService.createDeployment()
                .name("出差申请2")
                .addClasspathResource("flow/evection.bpmn20.xml")
                .addClasspathResource("flow/evection.png")
                .deploy();
        System.out.println("流程部署的id： "+deploy.getId());
        System.out.println("流程部署的name： "+deploy.getName());

    }

    @Test
    public void satrt(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("evection");
        System.out.println("流程定义id： " + processInstance.getProcessDefinitionId());
        System.out.println("流程实例id： " + processInstance.getId());
        System.out.println("当前活动的id： " + processInstance.getActivityId());

    }


    // 查询个人得执行的任务
    @Test
    public  void  qureyTask(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("evection")
                .taskAssignee("zhangsan")
                .list();

        for (Task task : list) {
            System.out.println("流程实例id： "+task.getProcessInstanceId());
            System.out.println("任务id： "+task.getId());
            System.out.println("任务负责人： "+task.getAssignee());
            System.out.println("任务名称： "+task.getName());
        }

//        select distinct RES.* from ACT_RU_TASK RES inner join ACT_RE_PROCDEF D
//        on RES.PROC_DEF_ID_ = D.ID_ WHERE RES.ASSIGNEE_ = 'zhangsan' and D.KEY_ = 'evection'
//        order by RES.ID_ asc LIMIT 2147483647 OFFSET 0

    }


    //删除流程部署信息
    // 如果流程没有执行完 删除是会报错 如果非要删除就得使用特殊的方法 原理是使用级联删除
    @Test
    public  void deleteProcess(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
//        repositoryService.deleteDeployment("10001");

        repositoryService.deleteDeployment("10001",true); // 使用级联删除
    }


    // 查看历史是信息
    @Test
    public void findHisInfo(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        HistoryService historyService = processEngine.getHistoryService();
        HistoricActivityInstanceQuery instanceQuery = historyService.createHistoricActivityInstanceQuery();
        instanceQuery.processInstanceId("12501");
        instanceQuery.orderByHistoricActivityInstanceStartTime().asc();
        List<HistoricActivityInstance> list = instanceQuery.list();
        for (HistoricActivityInstance hi : list) {
            System.out.println(hi.getActivityId());
            System.out.println(hi.getActivityName());
            System.out.println(hi.getProcessDefinitionId());
            System.out.println(hi.getProcessInstanceId());
            System.out.println("==============================");

        }

    }



}
