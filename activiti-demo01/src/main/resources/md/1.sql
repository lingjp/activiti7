select * from act_ge_property;

select* From act_re_deployment;  # 流程定义部署表，每部署一次增加一条记录
select* From act_ge_bytearray;    # 流程资源表 

select* From act_re_procdef;     #   流程定义表，部署每个新的流程定义都会在这张表中增加一条记录


select* From act_ru_task;           #任务信息  目前执行到哪了 结束后会删除
select* From act_ru_execution;   		#流程执行信息 ， 如果和业务表联系  其中的 BUSINESS_KEY_ 对应业务的id
select* From act_ru_identitylink;  	#流程的参与用户信息
2024-04-26 01:21:54.157

select* From act_hi_actinst;     		#流程实例执行历史 详细的执行信息 执行完不会删除
select* From act_hi_identitylink;  	#流程的参与用户历史信息
select* From act_hi_procinst;				#流程实例历史信息
select* From act_hi_taskinst;      	#流程任务历史信息


SELECT DISTINCT
	RES.* 
FROM
	ACT_RU_TASK RES 
WHERE
	RES.PROC_DEF_ID_ = 'test1:1:3'
ORDER BY
	RES.ID_ ASC 
	LIMIT ? OFFSET ?